/*
 * ChapterNode.java
 *
 * Created on April 12, 2007, 1:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.graph.impl.DirectedSparseVertex;
import edu.uci.ics.jung.utils.UserDataContainer;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author ckirby
 */
public class ChapterNode {
    private HashMap<String, Integer> targetSize;
    private Vertex vertex;
    private int radius;
    private String name;
    /** Creates a new instance of ChapterNode */
    public ChapterNode(String name) {
        this.name = name;
        this.targetSize = new HashMap<String, Integer>();
        this.vertex = new DirectedSparseVertex();
        this.radius = 0;
    }
    
    public void addStatute(String target){
        if(this.targetSize.get(target) == null){
            targetSize.put(target, 1);
        }else{
            targetSize.put(target, targetSize.get(target)+1) ;
        }
    }
    
    public Vertex getVertex(){
        this.vertex.setUserDatum(VertexAttributes.NAME_DATUM, getName(), new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.TYPE,VertexAttributes.CHAPTER_TYPE, new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.RADIUS,this.radius, new UserDataContainer.CopyAction.Remove());
        
        return this.vertex;
    }
    
    private String getName(){
        return this.name;
    }
    
    public HashSet<String> getTargets(){
        return new HashSet<String>(targetSize.keySet());
    }
    
    public int getTargetWeight(String target){
        return targetSize.get(target).intValue();
    }
    
    public void addNode (){
        this.radius++;
    }
    
}
