/*
 * GraphAttributes.java
 *
 * Created on April 6, 2007, 3:10 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;


/**
 *
 * @author ckirby
 */
public class GraphAttributes {
    private static LinkedList<String> chaps = new LinkedList<String>();
    private static LinkedList<String> subs = new LinkedList<String>();
    private HashMap<String, GAStats> attributes = new HashMap<String, GAStats>();
    private static int totalStatutes;
    private static Dimension nodeSize = null;
    private static int maxX = 100;
    private static int maxY = 100;
    private static int currentY = 100;
    private static final int GRAPH_MULTIPLIER = 500;
    private static final int WIDTH = 32;
    private static final int HEIGHT = 20;
    public static final int GRAPH_WIDTH = WIDTH * GRAPH_MULTIPLIER;
    public static final int GRAPH_HEIGHT = HEIGHT * GRAPH_MULTIPLIER;
    
    
    /**
     * Creates a new instance of GraphAttributes
     */
    
    public static LinkedList<String> getChaps(){
        return chaps;
    }
    
    public static LinkedList<String> getSubs(){
        return subs;
    }
    
    public void addChap(String chap){
        chaps.add(chap);
        attributes.put(chap, new GAStats());
    }
    
    public void addSub(String sub){
        subs.add(sub);
    }
    
    public void addStatute(String chap){
        attributes.get(chap).addStatute();
        totalStatutes++;
    }
    
    private Dimension getNodeSize(){
        if(nodeSize == null){
            double nodeArea = (WIDTH * HEIGHT * GRAPH_MULTIPLIER)/((Integer)totalStatutes).doubleValue();
            
            Double nodeWidth = (nodeArea/HEIGHT)*1.2;
            Double nodeHeight = (nodeArea/WIDTH)*1.2;
            
            nodeSize = new Dimension(nodeWidth.intValue(), nodeHeight.intValue());
        }
        
        return nodeSize;
    }
    
    public Dimension getChapterDimension(String chap){
        GAStats gas = attributes.get(chap);
        Dimension nodeDimension = getNodeSize();
        if(gas.bounds == null){            
            Double width = (gas.size) * ((Integer)nodeDimension.width).doubleValue();
            Double height = (gas.size) * ((Integer)nodeDimension.height).doubleValue();
            gas.bounds = new Dimension(width.intValue()+(gas.size/5)+80, height.intValue()+(gas.size/8)+50);
        }
        
        
        
        return gas.bounds;
    }
    
    public Dimension getChapterStarts(String chap){
        GAStats gas = attributes.get(chap);
        if(gas.meets == null){
            Dimension chapDimension = getChapterDimension(chap);
            int x = 0;
            int y = 0;
            if(maxX + chapDimension.width < GRAPH_WIDTH){
                x = maxX;
                y = currentY;
                maxX = maxX + chapDimension.width+320;
                maxY = Math.max(y + chapDimension.height, maxY);
                if(maxY == y + chapDimension.height){
                    maxY += 200;
                }
            }else{
                x = 100;
                maxX = chapDimension.width+50;
                y = maxY;
                currentY = maxY;
                maxY = (currentY + chapDimension.height);
            }
            
            gas.meets = new Dimension(x, y);
        }

        return gas.meets;
    }
    
    public Color getChapColor(String chap){
        int index = chaps.indexOf(chap);
        Color color = new Color(Color.HSBtoRGB((200-(index*3f))/360f, 1f, 1f));
        
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), 150);
    }
    
    public Color getSubColor(String sub){
        int index = subs.indexOf(sub);
        Color color = new Color(Color.HSBtoRGB(((index*18f)+20)/360f, 1f, 1f));
        
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), 200);
    }
    
    private class GAStats{
        public int size;
        
        public Dimension bounds = null;
        
        public Dimension meets = null;
        
        public GAStats(){}
        
        public void addStatute(){
            size++;
        }
               
    }
    
}
