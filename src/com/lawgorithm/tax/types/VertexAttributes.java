/*
 * VertexAttributes.java
 *
 * Created on April 6, 2007, 2:54 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

/**
 *
 * @author ckirby
 */
public class VertexAttributes {
    public static final String INPUTS_SIZE_DATUM = "inputs";
    public static final String OUTPUTS_SIZE_DATUM = "outputs";
    public static final String NAME_DATUM = "name";
    public static final String SUB_DATUM = "subtitle";
    public static final String CHAP_DATUM = "chapter";
    public static final String TYPE = "type";
    public static final String CHAPTER_TYPE = "chapter";
    public static final String STATUTE_TYPE = "statute";
    public static final String RADIUS = "radius";
    
}
