/*
 * StatuteNode.java
 *
 * Created on March 2, 2007, 7:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

import com.sun.org.apache.xerces.internal.dom.NodeImpl;
import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.graph.impl.DirectedSparseVertex;
import edu.uci.ics.jung.utils.UserDataContainer;
import edu.uci.ics.jung.utils.UserDataContainer.CopyAction;
import edu.uci.ics.jung.utils.UserDataContainer.CopyAction.Clone;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ckirby
 */
public class StatuteNode {
    private String name;
    private String sub;
    private String chap;
    private List<String> inputs;
    private List<String> targets;
    private Vertex vertex;
    
    /** Creates a new instance of StatuteNode */
    public StatuteNode(String name, String node, String sub, String chap){
        this.name = name;
        this.inputs = new LinkedList<String>();
        if((node != null) && (node != name)){
            this.inputs.add(node);
        }
        this.targets = new LinkedList<String>();
        this.vertex = new DirectedSparseVertex();
        this.sub = sub;
        this.chap = chap;
    }
    
    public Vertex getVertex(){
        this.vertex.setUserDatum(VertexAttributes.TYPE,VertexAttributes.STATUTE_TYPE, new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.INPUTS_SIZE_DATUM, getInputsCount(), new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.OUTPUTS_SIZE_DATUM, getOutputsCount(), new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.NAME_DATUM, getName(), new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.SUB_DATUM, getSub(), new UserDataContainer.CopyAction.Remove());
        this.vertex.setUserDatum(VertexAttributes.CHAP_DATUM, getChap(), new UserDataContainer.CopyAction.Remove());
        
        return this.vertex;
    }
    
    public void addTarget(String target){
        if(!this.targets.contains(target)){
            this.targets.add(target);
        }
    }
    
    public void addTarget(String[] targets){
        for(String target : targets){
            if(!this.targets.contains(target)){
                this.targets.add(target);
            }
        }
    }
    
    public List<String> getTargets(){
        return this.targets;
    }
    
    public void addInput(String node){
        if((!this.inputs.contains(node)) && (node != this.name)){
            this.inputs.add(node);
        }
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getSub(){
        return this.sub;
    }
    
    public String getChap(){
        return this.chap;
    }
    
    public int getInputsCount(){
        return this.inputs.size();
    }
    
    public int getOutputsCount(){
        return this.targets.size();
    }
}
