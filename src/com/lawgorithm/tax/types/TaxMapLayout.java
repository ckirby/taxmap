/*
 * MyFRLayout.java
 *
 * Created on April 6, 2007, 2:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

import com.lawgorithm.tax.TaxMap;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.visualization.Coordinates;
import edu.uci.ics.jung.visualization.StaticLayout;
import java.awt.Dimension;

/**
 *
 * @author ckirby
 */
public class TaxMapLayout extends StaticLayout{
    private GraphAttributes ga = null;
    public TaxMapLayout(Graph g, GraphAttributes ga){
        super(g);
        this.ga = ga;
    }
    
    protected void initializeLocation(Vertex v, Coordinates coord, Dimension d) {
        String chap = (String)v.getUserDatum(VertexAttributes.CHAP_DATUM);
        Dimension bounds = ga.getChapterStarts(chap);
        Dimension size = ga.getChapterDimension(chap);
        
        double x = (Math.random() * size.width) + bounds.width;
        double y = (Math.random() * size.height) + bounds.height;
        coord.setX(x);
        coord.setY(y);
    }   
}
