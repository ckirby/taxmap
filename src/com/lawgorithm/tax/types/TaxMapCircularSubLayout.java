/*
 * TaxMapCircularSubLayout.java
 *
 * Created on April 12, 2007, 10:50 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax.types;

import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.visualization.subLayout.CircularSubLayout;
import java.awt.geom.Point2D;
import java.util.Collection;

/**
 *
 * @author ckirby
 */
public class TaxMapCircularSubLayout extends CircularSubLayout{
    
    /** Creates a new instance of TaxMapCircularSubLayout */
    public TaxMapCircularSubLayout(Collection vertices, double radius, Point2D center) {
        super(vertices, radius, center);
        this.initializeLocations(vertices);
    }
    
    public void initializeLocations(Collection vertices) {
        Vertex[] vertexArray = (Vertex[]) vertices.toArray(new Vertex[vertices.size()]);

        if(center == null) {
            center = new Point2D.Double(radius, radius);
        }
        // only apply sublayout if there is more than one vertex
        if (vertexArray.length > 1) {
            //if((vertexArray.length > 120))
            for (int i = 0; i < vertexArray.length; i++) {
                if(((Vertex)vertexArray[i]).getUserDatum(VertexAttributes.TYPE) == VertexAttributes.CHAPTER_TYPE){
                    map.put(vertexArray[i], center);
                }else{
                    double angle = (2 * Math.PI * i) / vertexArray.length;
                    //Point2D point = new Point2D.Double((Math.cos(angle) * radius + center.getX()), (Math.sin(angle)* radius + center.getY()));
                    //Point2D point = new Point2D.Double(Math.random()*radius+ center.getX(), Math.random()*radius+ center.getY());
                    Point2D point = new Point2D.Double((Math.cos(angle) * (Math.random()*radius) + center.getX()), (Math.sin(angle)* (Math.random()*radius) + center.getY()));
                    map.put(vertexArray[i], point);
                }
            }
        }
//        else{
//                for (int i = 0; i < vertexArray.length; i++) {
//				double angle = (2 * Math.PI * i) / vertexArray.length;
//				Point2D point = new Point2D.Double(
//						(Math.cos(angle) * radius + center.getX()), (Math
//								.sin(angle)
//								* radius + center.getY()));
//				map.put(vertexArray[i], point);
//			}
//                        
//            }
        }
        
    }
    
