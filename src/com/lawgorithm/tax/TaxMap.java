/*
 * TaxMap.java
 *
 * Created on March 2, 2007, 7:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.lawgorithm.tax;

import com.lawgorithm.tax.types.ChapterNode;
import com.lawgorithm.tax.types.GraphAttributes;
import com.lawgorithm.tax.types.TaxMapCircularSubLayout;
import com.lawgorithm.tax.types.TaxMapLayout;
import com.lawgorithm.tax.types.StatuteNode;
import com.lawgorithm.tax.types.VertexAttributes;
import edu.uci.ics.jung.algorithms.cluster.ClusterSet;
import edu.uci.ics.jung.algorithms.cluster.VertexClusterSet;
import edu.uci.ics.jung.exceptions.ConstraintViolationException;
import edu.uci.ics.jung.graph.ArchetypeVertex;
import edu.uci.ics.jung.graph.Edge;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.graph.decorators.AbstractVertexShapeFunction;
import edu.uci.ics.jung.graph.decorators.EdgePaintFunction;
import edu.uci.ics.jung.graph.decorators.EdgeShape;
import edu.uci.ics.jung.graph.decorators.EdgeShapeFunction;
import edu.uci.ics.jung.graph.decorators.EdgeStrokeFunction;
import edu.uci.ics.jung.graph.decorators.NumberVertexValue;
import edu.uci.ics.jung.graph.decorators.UserDatumNumberVertexValue;
import edu.uci.ics.jung.graph.decorators.VertexPaintFunction;
import edu.uci.ics.jung.graph.decorators.VertexSizeFunction;
import edu.uci.ics.jung.graph.decorators.VertexStringer;
import edu.uci.ics.jung.graph.decorators.VertexStrokeFunction;
import edu.uci.ics.jung.graph.impl.DirectedSparseEdge;
import edu.uci.ics.jung.graph.impl.DirectedSparseGraph;
import edu.uci.ics.jung.utils.Pair;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.FRLayout;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.ISOMLayout;
import edu.uci.ics.jung.visualization.PluggableRenderer;
import edu.uci.ics.jung.visualization.SpringLayout;
import edu.uci.ics.jung.visualization.StaticLayout;
import edu.uci.ics.jung.visualization.VisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.contrib.CircleLayout;
import edu.uci.ics.jung.visualization.contrib.KKLayout;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.LayoutScalingControl;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.control.ViewScalingControl;
import edu.uci.ics.jung.visualization.subLayout.CircularSubLayout;
import edu.uci.ics.jung.visualization.subLayout.SubLayout;
import edu.uci.ics.jung.visualization.subLayout.SubLayoutDecorator;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.EPSDump;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author ckirby
 */
public class TaxMap{
    private static HashMap<String, StatuteNode> nodes;
    private static HashMap<String, HashSet<Vertex>> chapVerts;
    private static HashMap<String, ChapterNode> chapNodes;
    private static GraphAttributes graphAT = new GraphAttributes();
    private static float zoomFactor = 1.5f;
    private static boolean showChaps = true;
    private static int shotCount = 0;
    
    /** Creates a new instance of TaxMap */
    public static void main(String[] args){
        TaxMap tm = new TaxMap();
        JFrame jf = new JFrame();
        Graph g = getGraph();
        
        //StaticLayout layout = new TaxMapLayout(g, graphAT);
        final SubLayoutDecorator layout = new SubLayoutDecorator(new StaticLayout(g));
        layout.initialize(new Dimension(graphAT.GRAPH_WIDTH, graphAT.GRAPH_HEIGHT));
        layout.removeAllSubLayouts();
        
        int chapCount = -1;
        Random randGen = new Random();
        List<CircleCollisionData> circles = new LinkedList<CircleCollisionData>();
        for(String chap : chapVerts.keySet()){
            if(chapVerts.get(chap).size() > 0){
                chapCount++;
                boolean done = false;
                Point2D center = null;
                while(!done){
                    double angle = (2 * Math.PI * chapCount) / 55;
                    center = new Point2D.Double(0.0,0.0);
                    double radX = randGen.nextDouble() * 2000 + 12000;
                    double radY = randGen.nextDouble() * 2000 + 8000;
                    if(chap.equals("1")){
                        center = new Point2D.Double((graphAT.GRAPH_WIDTH/2)+5000,(graphAT.GRAPH_HEIGHT/2)+5000);
                    }else{

                        double x = (Math.cos(angle) * radX +(graphAT.GRAPH_WIDTH/2)+5000);
                        double y = (Math.sin(angle) * radY +(graphAT.GRAPH_HEIGHT/2)+5000);
                        center = new Point2D.Double(x, y);
                    }
                    //Point2D center = layout.getLocation((ArchetypeVertex)chapVerts.get(chap).iterator().next()); 
                    double pointX = center.getX();
                    double pointY = center.getY();
                    if((pointX - chapVerts.get(chap).size()*6) < 100){
                        pointX = 100 + chapVerts.get(chap).size()*6;
                    }   
                    if((pointY - chapVerts.get(chap).size()*6) < 100){
                        pointY = 100 + chapVerts.get(chap).size()*6;

                    }
                    center.setLocation(pointX, pointY);
                    done = true;
                    for(CircleCollisionData circle : circles){
                        if((chapVerts.get(chap).size()*6 + circle.radius.doubleValue()) >= center.distance(circle.center)){
                            done = false;
                            break;
                        }
                    }
                }
                circles.add(new CircleCollisionData(center, chapVerts.get(chap).size()*6.0));
                HashSet vs = chapVerts.get(chap);
                vs.add(chapNodes.get(chap).getVertex());
                SubLayout subLayout = new TaxMapCircularSubLayout(vs, chapVerts.get(chap).size()*6, center); 
                layout.addSubLayout(subLayout);
            }
        }
        
        
        //final VisualizationModel visualizationModel = new DefaultVisualizationModel(layout, new Dimension(800,500));
        final VisualizationViewer vv = new VisualizationViewer(layout, getRenderer());
        vv.setBackground(Color.BLACK);
        
        final ScalingControl scaler = new ViewScalingControl();
        
        final GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(vv);

        JPanel allPanel = new JPanel(new BorderLayout());
        scrollPane.setSize(1600,1000);
        allPanel.add(scrollPane, BorderLayout.CENTER);
        
        JButton plus = new JButton("+");
        
        JTextField zoomFactorField = new JTextField("1.5", 5);
        zoomFactorField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String factor = ((JTextField)e.getSource()).getText();
                setZoomFactor(factor);
            }
        });
        plus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(vv, zoomFactor, new Point());//vv.getCenter());
            }
        });
        JButton minus = new JButton("-");
        minus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(vv, 1/zoomFactor, new Point());//vv.getCenter());
            }
        });
       
        
        JPanel buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.add(plus, BorderLayout.LINE_START);
        buttonPanel.add(minus, BorderLayout.LINE_END);
        buttonPanel.add(zoomFactorField, BorderLayout.CENTER);
        
        JButton savePNGButton = new JButton("Save PNG");
        savePNGButton.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               saveMapPNG(vv, "taxMap"+showChaps+shotCount);
               shotCount++;
           }
        });
        
        
        JButton switchButton = new JButton("Switch");
        switchButton.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               showChaps = !showChaps;
               vv.setRenderer(vv.getRenderer());
               vv.repaint();
           }
        });
        
        JPanel savePanel = new JPanel(new BorderLayout());
        savePanel.add(savePNGButton, BorderLayout.LINE_START);
        
        JPanel controlPanel = new JPanel(new BorderLayout());
        controlPanel.add(buttonPanel, BorderLayout.LINE_START);
        //buttonPanel.add(savePanel, BorderLayout.PAGE_START);
        controlPanel.add(savePanel, BorderLayout.LINE_END);
        controlPanel.add(switchButton, BorderLayout.CENTER);
        
        allPanel.add(controlPanel, BorderLayout.PAGE_START);
        //scaler.scale(vv, 7.5f, new Point());
        
        jf.setName("Taxmap");
        jf.setTitle("Taxmap");
        jf.getContentPane().add(allPanel);        
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.pack();
        jf.setSize(1600, 1000);
        jf.setVisible(true);

    
    }
    
    public static void setZoomFactor(String factor){
        zoomFactor = Float.parseFloat(factor);
    } 
    
    private static void saveMapPNG(VisualizationViewer vv, String name){ 
        int width = vv.getWidth(); 
        int height = vv.getHeight(); 
        Color bg = vv.getBackground(); 

        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR); 
        Graphics2D graphics = bi.createGraphics(); 
        graphics.setColor(bg); 
        graphics.fillRect(0, 0, width, height); 
        vv.paint(graphics); 

        try{ 
            ImageIO.write(bi, "png", new File(name+".png")); 
        }catch (IOException e){ 
            e.printStackTrace(); 
        } 
         
    }
    
    public TaxMap() {
        nodes = new HashMap<String, StatuteNode>();
        chapVerts = new HashMap<String, HashSet<Vertex>>(); 
        chapNodes = new HashMap<String, ChapterNode>();
        try{
            File taxFile = new File("tax_map.dat");
            BufferedReader br = new BufferedReader(new FileReader(taxFile));
            String line;
            String sub ="";
            String chap ="";
            
            while ((line = br.readLine()) != null){
                if(line.contains("SUB")){
                    sub = line.substring(3);
                    graphAT.addSub(sub);
                }else if(line.contains("CHAP")){
                    chap = line.substring(4);
                    graphAT.addChap(chap);
                    chapVerts.put(chap, new HashSet<Vertex>());
                    chapNodes.put(chap, new ChapterNode(chap));
                }else{
                    graphAT.addStatute(chap);
                    String[] links = line.split("-->");
                    String[] targets;
                    if(links.length == 1){
                        targets = new String[0];
                    }else{
                        targets = links[1].split(",");
                    }
                    if(!nodes.containsKey(links[0])){
                        StatuteNode sn = new StatuteNode(links[0], null, sub, chap);
                        sn.addTarget(targets);
                        nodes.put(links[0], sn);
                    }else{
                        nodes.get(links[0]).addTarget(targets);
                    }

                    for(String target : targets){
                        if(!nodes.containsKey(target)){
                            StatuteNode sn = new StatuteNode(target, links[0], sub, chap);
                            nodes.put(target, sn);
                        }else{
                            nodes.get(target).addInput(links[0]);
                        }
                    }
                }
            }
        }catch(FileNotFoundException e){
            System.out.println("Could not find file");
        }catch(IOException e){
            System.out.println("Problem with opening file");
        }
    }

    public static Graph getGraph(){
        Graph g = new DirectedSparseGraph();
        for(String node : nodes.keySet()){
            g.addVertex(nodes.get(node).getVertex());
            chapVerts.get(nodes.get(node).getChap()).add(nodes.get(node).getVertex());
            chapNodes.get(nodes.get(node).getChap()).addNode();
        }
        for(String node : chapNodes.keySet()){
            if(((Integer)chapNodes.get(node).getVertex().getUserDatum(VertexAttributes.RADIUS)).intValue() != 0){
                g.addVertex(chapNodes.get(node).getVertex());
            }
        }
        for(String node : nodes.keySet()){
            List<String> ends = nodes.get(node).getTargets();
            for(String end : ends){
                DirectedSparseEdge edge = null;
                try{
                    if(!(nodes.get(node).getName() == nodes.get(end).getName())){
//                        if(((String)nodes.get(node).getVertex().getUserDatum(VertexAttributes.CHAP_DATUM)) != ((String)nodes.get(end).getVertex().getUserDatum(VertexAttributes.CHAP_DATUM))){
//                            chapNodes.get((String)nodes.get(node).getVertex().getUserDatum(VertexAttributes.CHAP_DATUM)).addStatute((String)nodes.get(end).getVertex().getUserDatum(VertexAttributes.CHAP_DATUM));
//                        }else{
                            edge = new DirectedSparseEdge(nodes.get(node).getVertex(), nodes.get(end).getVertex());
                            g.addEdge(edge);
                        //}
                    }
                }catch(ConstraintViolationException e){
                    System.out.println(nodes.get(node).getName()+":"+nodes.get(end).getName());
                }
            }
        }
//        for(String node : chapNodes.keySet()){
//            HashSet<String> ends = chapNodes.get(node).getTargets();
//            for(String end : ends){
//                DirectedSparseEdge edge = null; 
//                edge = new DirectedSparseEdge(chapNodes.get(node).getVertex(), chapNodes.get(end).getVertex());
//                g.addEdge(edge);
//            }
//        }
        return g;
    }

    private static PluggableRenderer getRenderer(){
        PluggableRenderer pr = new PluggableRenderer();
        pr.setVertexStringer(new VertexStringer(){
            public String getLabel(ArchetypeVertex v){
//                if(v.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.STATUTE_TYPE){
//                    StringBuffer sb = new StringBuffer("Chapter ");
//                    sb.append((String)v.getUserDatum(VertexAttributes.CHAP_DATUM));
//                    sb.append(" Sec. ");
//                    sb.append((String)v.getUserDatum(VertexAttributes.NAME_DATUM));
//                    return sb.toString();
//                }
                return "";
            }
        });
        pr.setVertexLabelCentering(true);
        pr.setVertexShapeFunction(new VertexShapeSizeAspect(new UserDatumNumberVertexValue(VertexAttributes.INPUTS_SIZE_DATUM)));
        pr.setVertexPaintFunction(new VertexPaintFunction(){
            public Paint getFillPaint(Vertex v) {
                if(v.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.STATUTE_TYPE){
                    int blue = (Integer)v.getUserDatum(VertexAttributes.INPUTS_SIZE_DATUM);
                    int red = (Integer)v.getUserDatum(VertexAttributes.OUTPUTS_SIZE_DATUM);
                    return new Color((float)red/(red+blue), 0.0f, (float)blue/(red+blue));
                }else{
                    //return Color.ORANGE;
                    if(showChaps){
                        return new Color(150,150,150,80);
                    }else{
                        return new Color(0,0,0,0);
                    }
                }
            }            
            public Paint getDrawPaint(Vertex v) {
                if(v.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.STATUTE_TYPE){
                    int ins = (Integer)v.getUserDatum(VertexAttributes.INPUTS_SIZE_DATUM);
                    int outs = (Integer)v.getUserDatum(VertexAttributes.OUTPUTS_SIZE_DATUM);
                    if(ins == 1){
                        return Color.GREEN;
                    }else if(outs == 1){
                        return Color.YELLOW;
                    }
                    return Color.BLACK;
                }else{
                    return new Color(0,0,0,0);
                }
            }           
        });
        pr.setVertexStrokeFunction(new VertexStrokeFunction(){
           public Stroke getStroke(Vertex v){
                if(v.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.CHAPTER_TYPE){
                    return new BasicStroke(6f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
                }               
                return new BasicStroke(3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
           } 
        });
        pr.setEdgePaintFunction(new EdgePaintFunction(){
            private Color unselected = new Color(1.0f,1.0f,1.0f,0.6f);
            public Paint getFillPaint(Edge e){
                Pair points = e.getEndpoints();
                Vertex first = (Vertex)points.getFirst();
                Vertex second = (Vertex)points.getSecond();
                if(first.getUserDatum(VertexAttributes.CHAP_DATUM) != second.getUserDatum(VertexAttributes.CHAP_DATUM)){
                    return graphAT.getChapColor((String)first.getUserDatum(VertexAttributes.CHAP_DATUM));
                }
                return unselected;
            }
            public Paint getDrawPaint(Edge e){
                Pair points = e.getEndpoints();
                Vertex first = (Vertex)points.getFirst();
                Vertex second = (Vertex)points.getSecond();
                //if(first.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.CHAPTER_TYPE){
                if(first.getUserDatum(VertexAttributes.CHAP_DATUM) != second.getUserDatum(VertexAttributes.CHAP_DATUM)){
                    return graphAT.getChapColor((String)first.getUserDatum(VertexAttributes.CHAP_DATUM));
                }
                return unselected;
            }
        });
        pr.setEdgeShapeFunction(new EdgeShape.Line());
        pr.setEdgeStrokeFunction(new EdgeStrokeFunction(){
           public Stroke getStroke(Edge e){
               Pair points = e.getEndpoints();
                Vertex first = (Vertex)points.getFirst();
                Vertex second = (Vertex)points.getSecond();
                //if(first.getUserDatum(VertexAttributes.TYPE) != VertexAttributes.CHAPTER_TYPE){
                if(first.getUserDatum(VertexAttributes.CHAP_DATUM) != second.getUserDatum(VertexAttributes.CHAP_DATUM)){
                    return new BasicStroke(6f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
                }
                return new BasicStroke(3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
                
                //return new BasicStroke(chapNodes.get(first.getUserDatum(VertexAttributes.NAME_DATUM)).getTargetWeight((String)second.getUserDatum(VertexAttributes.NAME_DATUM))*6f,BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
           } 
        });
        return pr;
    }

    private final static class VertexShapeSizeAspect extends AbstractVertexShapeFunction implements VertexSizeFunction{
        protected NumberVertexValue inputs;
        
        public VertexShapeSizeAspect(NumberVertexValue inputs){
            this.inputs = inputs;
            setSizeFunction(this);
        }
        
        public int getSize(Vertex v){
            if(v.getUserDatum(VertexAttributes.TYPE) == VertexAttributes.STATUTE_TYPE){
                return (this.inputs.getNumber(v).intValue()+1)*5;
            }else{
                return Math.max(((Integer)v.getUserDatum(VertexAttributes.RADIUS)).intValue()*12, 200);
            }
        }
        
        public Shape getShape(Vertex v){
            return factory.getEllipse(v);
        }   
    }
    
    private final static class CircleCollisionData{
        protected Double radius;
        protected Point2D center;
        
        public CircleCollisionData(Point2D center, Double radius){
            this.center = center;
            this.radius = radius;
        }
    }
}
